
Integrates Drupal's mail system with Sendwithus' (sendwithus.com)
transaction email API.  Sendwithus allows templating, analytics, split
testing, and drip campaign features to transaction emails. Emails can be
setup to be delivered through Sendgrid, AWS SES, Mailgun, and other
popular email sending services through the Sendwithus dashboard.


Installation ------------

Download the Sendwithus PHP  library found here:
https://github.com/sendwithus/sendwithus_php and extract it to
sites/all/libraries/sendwithus, so that API.php is found under
sites/all/libraries/sendwithus/lib

Copy this to your module directory and then enable on the admin modules
page.  Enter your Sendwithus API Key and set a default template ID at
admin/config/services/sendwithus.  

Make sure your default email template
in the Sendwithus dashboard has a %message% variable somewhere in the
template body. Choose SendwithusMailSystem as your default site-wide
default class at admin/config/system/mailsystem.


Usage -------------

After installation, this module will send all standard emails using the
Sendwithus service -- which offers many features out-of-the-box.  To
choose a custom email template, include a 'sendwithus_email_id'
paramater in your custom module's hook_mail function.
