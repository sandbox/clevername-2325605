<?php

/**
 * @file
 * Implements Sendwithus as a Drupal MailSystemInterface.
 */

/**
 * Modify the drupal mail system to use Sendwithus when sending emails.
 */
class SendwithusMailSystem implements MailSystemInterface {

  /**
   * Concatenate and wrap the email body for either plain-text or HTML emails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    if (is_array($message['body'])) {
      $message['body'] = implode("\n\n", $message['body']);
    }
    return $message;
  }

  /**
   * Send the email message.
   *
   * @see drupal_mail()
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {
    // Use email ID included in array if set, otherwise use default.
    $email_id = !empty($message['params']['sendwithus_email_id']) ? $message['params']['sendwithus_email_id'] : variable_get('sendwithus_default_email_id');
    $recipient = array(
     	'address' => $message['to'],
     	'name'	=>	(!empty($message['params']['realname']) ? $message['params']['realname'] : NULL),
    );
    $options = array(
     	'email_data' => $message['params'],
    );

    return sendwithus_send($email_id, $recipient, $options);
  }

}
